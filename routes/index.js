var express = require('express');
var router = express.Router();

router.get('/time', function(req, res, next) {
  var response = {
    epoch: Date.now(), 
  };
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(response));
});

module.exports = router;
