# Demo App Backend
An express.js API which returns the server time and performance metrics.

## How to debug
To run the server on localhost run the following command:

`npm start`


## Debug CURL commands
To test the `/time` endpoint:

`curl -H "Authorization: mysecrettoken" localhost:3000/time`


To test the `/metrics` endpoint:

`curl -H "Authorization: mysecrettoken" localhost:3000/metrics`
